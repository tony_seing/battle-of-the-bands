class Song < ActiveRecord::Base
  belongs_to :band
  has_attached_file :audio

end
