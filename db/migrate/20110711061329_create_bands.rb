class CreateBands < ActiveRecord::Migration
  def self.up
    create_table :bands do |t|
      t.string :name
      t.text :description
      t.text :website
      t.string :facebook
      t.string :myspace
      t.string :location

      t.timestamps
    end
  end

  def self.down
    drop_table :bands
  end
end
